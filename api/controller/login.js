const models=require('../models');
const jwt=require('jsonwebtoken');
const sequelize = models.sequelize;

exports.token=async (req,res)=>{
    const {userPhoneNumber,userPassword}=req.body;
  let checkUser = await models.user.findOne({ where: { userPhoneNumber: userPhoneNumber,userPassword:userPassword, isActive: true } });
  if (!checkUser) {
    return res.status(404).json({ message: 'User not found' })
}
console.log(checkUser);
const token =jwt.sign({
    id:checkUser.dataValues.id,

},'manish')
res.send({message:'your token ',token:token}); 

};
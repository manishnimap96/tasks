const models=require('../models');
const sequalize=models.sequelize;
const Sequalize=models.Sequelize;
const OP=Sequalize.Op;

//add user

exports.addUser=async (req,res)=>{
   try{

        const{
        userFirstName,
        userLastName,
        userPhoneNumber,
        userPassword,
        isActive
    }=req.body;
    
    let user=await models.user.create({
        userFirstName,
        userLastName,
        userPassword,
        userPhoneNumber,
        isActive
    });
    if (!user) {
        res.status(422).json({ message: "data not created" });
      }
      console.log(user);
      res.status(201).json(user);
}
catch(ex){
    console.log(ex)
}
}
exports.getUser=async (req,res)=>{
    try{

        if(req.query)

let data=await models.user.findAll();
if(!data){return res.send({message:'Data not found'})};
res.send(data);
    }
    catch(ex){
        res.send(ex.message);
    }
}

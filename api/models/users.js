module.exports=(sequelize,DataTypes)=>{
    const User=sequelize.define('user',{
        userFirstName:{
            type:DataTypes.STRING,
        },
        userLastName:{
            type:DataTypes.STRING
        },  
        
        userPhoneNumber:{
            type:DataTypes.BIGINT()
            },
    
            userPassword:{
                type:DataTypes.TEXT()
            },
            
        isActive:{
            type:DataTypes.BOOLEAN,
                defaultValue:true  
                }  
            },
    
        {
            timestimp:false,
            freezeTableName: true,
            tableName: 'user',
        } 
    );
    return User;
    }


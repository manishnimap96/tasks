const express=require('express');
const route=express.Router();


const user=require('../controller/users');

route.post('/',user.addUser);
route.get('/',user.getUser);


module.exports=route;
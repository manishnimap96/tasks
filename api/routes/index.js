var express=require('express');
var router=express.Router();

var user=require('./users');
var token=require('./login')

router.use('/register',user);
router.use('/user',token);


module.exports=router;
    